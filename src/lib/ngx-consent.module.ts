import { ModuleWithProviders, NgModule } from '@angular/core';
import { NgxConsentComponent } from './ngx-consent.component';
import { NgxConsentService } from './ngx-consent.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormioModule } from '@formio/angular';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from '@universis/common';

@NgModule({
  declarations: [
    NgxConsentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FormioModule,
    SharedModule,
    ModalModule
  ],
  exports: [
    NgxConsentComponent
  ],
  entryComponents: [
    NgxConsentComponent
  ]
})
export class NgxConsentModule {
  
  static forRoot(): ModuleWithProviders<NgxConsentModule> {
    return {
      ngModule: NgxConsentModule,
      providers: [
        NgxConsentService
      ]
    };
  }

}
